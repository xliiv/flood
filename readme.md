# Calc flood

> Program that calculates the water level in different parts of a landscape.


# Problem

Then it begins to rain. Per hour one unit of rain falls on each segment of the
landscape.  Water that falls on segment 3 in the above sample landscape will
flow equally into segment 2 and into segment 4, until segment 4 is filled up to
the level of segment 3.  Water that falls on segment 5 will flow into segment
4, because segment 6 is higher.  Water that falls on segment 6 will flow into
segment 5 (and then further into segment 4) because right to segment 6 is an
infinite wall (same as left to segment 1).  To the very left and to the very
right of the landscape are infinite walls, i.e. water does not flow outside of
the defined landscape.  The program shall calculate the water levels for each
segment after x number of hours of rain.


# TODO

* [x] describe an algorithm (in text form)
* [ ] prove that the algorithm works
* [x] implement the algorithm in a programming language of your choice


# Algorithm

1. for each increase of water (IOW)
    1. for each segment S
        1. find depression D (lowest segment) in the neighbour of S (left side of S and right side of S)
        2. if depression D is on both sides of S, store it in new list BSL
        3. if depression D is equal to S or D is only on one side (left or right of S) add delta to all segements D
        4. delta is equal to 1/depression-segments
    2. for each S from BLS
        1. find depression D (lowest segment) in the neighbour of S (left side of S and right side of S)
        3. add delta to all segements of D
        4. delta is equal to 1/depression-segments


This is the 2nd version of the algorithm. A lot of room for improvments in the
future.


# Code Status

* [x] Make it work
* [ ] Make it right
* [ ] Make it fast

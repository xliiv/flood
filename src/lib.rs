use anyhow::Result;

fn find_lowest_idx(landscape: &[f64], current: usize) -> (bool, Vec<usize>) {
    dbg!(&landscape[current]);
    dbg!(&landscape);
    if landscape.len() == 1 {
        return (false, vec![current]);
    }

    let mut right_lows = Vec::new();
    let mut right_prev = &landscape[current];
    for idx in current + 1 .. landscape.len() {
        let elem = &landscape[idx];
        if elem > right_prev {
            break;
        }
        if elem == right_prev {
            right_prev = elem;
            right_lows.push(idx);
            continue;
        }
        if elem < right_prev {
            right_prev = elem;
            right_lows = vec![idx];
        }
    }

    let mut left_lows = Vec::new();
    let mut left_prev = &landscape[current];
    let mut idx = current;
    while idx > 0 {
        idx -= 1;
        let elem = &landscape[idx];
        if elem > left_prev {
            break;
        }
        if elem == left_prev {
            left_prev = elem;
            left_lows.insert(0, idx);
            continue;
        }
        if elem < left_prev {
            left_prev = elem;
            left_lows = vec![idx];
        }
    }

    let mut result = Vec::new();
    if *left_prev < landscape[current] {
        // TODO: fix clone
        result.extend(left_lows.clone());
    }
    if *right_prev < landscape[current] {
        // TODO: fix clone
        result.extend(right_lows.clone());
    }
    let tower = *left_prev < landscape[current] && *right_prev < landscape[current];
    if result.len() == 0 {
        result.extend(left_lows);
        result.push(current);
        result.extend(right_lows);
    }
    dbg!(&result);
    return (tower, result);
}


fn calc_step(mut landscape: Vec<f64>) -> Vec<f64> {
    let segment_count = landscape.len();
    let mut split_segments = Vec::new();
    for idx in 0..segment_count {
        let (tower, low_indexes) = find_lowest_idx(&landscape, idx);
        let lower_segments_count = low_indexes.len();
        if tower {
            split_segments.push(idx);
            continue
        }
        for i in low_indexes {
            landscape[i] += 1./lower_segments_count as f64;
        }
        dbg!(&landscape);
    }
    for segment in split_segments.iter() {
        let (_, low_indexes) = find_lowest_idx(&landscape, segment.clone());
        let lower_segments_count = low_indexes.len();
        for i in low_indexes {
            landscape[i] += 1./lower_segments_count as f64;
        }
    }
    landscape
}

pub fn calc_water_level(time: usize, landscape: Vec<f64>) -> Result<Vec<f64>> {
    // TODO: validate data
    let mut step_result = landscape;
    for _n in 0..=time - 1 {
        step_result = calc_step(step_result);
    }
    Ok(step_result)
}

#[cfg(test)]
mod test_find_lowest_idx {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(find_lowest_idx(&vec![0.], 0), (false, vec![0]));
    }

    #[test]
    fn test2() {
        assert_eq!(find_lowest_idx(&vec![0., 1.], 0), (false, vec![0]));
    }

    #[test]
    fn test3() {
        assert_eq!(find_lowest_idx(&vec![0., 1.], 1), (false, vec![0]));
    }

    #[test]
    fn test4() {
        assert_eq!(find_lowest_idx(&vec![1., 0.], 0), (false, vec![1]));
    }

    #[test]
    fn test5() {
        assert_eq!(find_lowest_idx(&vec![1., 0.], 1), (false, vec![1]));
    }

    #[test]
    fn test6() {
        assert_eq!(find_lowest_idx(&vec![0., 0., 0.], 1), (false, vec![0, 1, 2]));
    }

    #[test]
    fn test7() {
        assert_eq!(find_lowest_idx(&vec![0., 1., 0.], 1), (true, vec![0, 2]));
    }

    #[test]
    fn test8() {
        assert_eq!(find_lowest_idx(&vec![0., 1., 1.], 1), (false, vec![0]));
    }

    #[test]
    fn test9() {
        assert_eq!(find_lowest_idx(&vec![1., 1., 0.], 1), (false, vec![2]));
    }

    #[test]
    fn test10() {
        assert_eq!(find_lowest_idx(&vec![1., 0., 1.], 1), (false, vec![1]));
    }

    #[test]
    fn test11() {
        assert_eq!(find_lowest_idx(&vec![0., 0., 1.], 1), (false, vec![0, 1]));
    }

    #[test]
    fn test12() {
        assert_eq!(find_lowest_idx(&vec![1., 0., 0.], 1), (false, vec![1, 2]));
    }

    #[test]
    fn test13() {
        assert_eq!(find_lowest_idx(&vec![0., 0., 1., 0., 0.], 2), (true, vec![0, 1, 3, 4]));
    }

    #[test]
    fn test14() {
        assert_eq!(find_lowest_idx(&vec![0., 1., 2., 0., 1.], 2), (true, vec![0, 3]));
    }

    #[test]
    fn test15() {
        assert_eq!(find_lowest_idx(&vec![0., 0., 2., 1., 1.], 2), (true, vec![0, 1, 3, 4]));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test0() {
        let output = calc_water_level(1, vec![]).unwrap();
        assert_eq!(output, vec![]);
    }

    #[test]
    fn test1() {
        let output = calc_water_level(1, vec![0.]).unwrap();
        assert_eq!(output, vec![1.]);
    }

    #[test]
    fn test2() {
        let output = calc_water_level(1, vec![0., 0., 0.]).unwrap();
        assert_eq!(output, vec![1., 1., 1.]);
    }

    #[test]
    fn test3() {
        let output = calc_water_level(3, vec![0., 0., 0.]).unwrap();
        assert_eq!(output, vec![3., 3., 3.]);
    }

    #[test]
    fn test4() {
        let output = calc_water_level(1, vec![0., 2.]).unwrap();
        assert_eq!(output, vec![2., 2.]);
    }

    #[test]
    fn test5() {
        let output = calc_water_level(1, vec![3.0, 1.0, 6.0, 4.0, 8.0, 9.0]).unwrap();
        assert_eq!(output, vec![4.0, 4.0, 6.0, 6.0, 8.0, 9.0]);
    }

    #[test]
    fn test6() {
        let output = calc_water_level(1, vec![1., 2., 3., 4., 5., 6., 7., 8., 9.]).unwrap();
        assert_eq!(output, vec![4.75, 4.75, 4.75, 4.75, 5., 6., 7., 8., 9.,]);
    }

    #[test]
    fn test7() {
        let output = calc_water_level(1, vec![0., 3., 2., 4.]).unwrap();
        assert_eq!(output, vec![3., 3., 3., 4.]);
    }

    #[test]
    fn test8() {
        let output = calc_water_level(1, vec![0., 3., 2., 2., 4.]).unwrap();
        assert_eq!(output, vec![3., 3., 3., 3., 4.]);
    }

    #[test]
    fn test9() {
        let output = calc_water_level(1, vec![0., 3., 2., 2., 4.]).unwrap();
        assert_eq!(output, vec![3., 3., 3., 3., 4.]);
    }

    #[test]
    fn test10() {
        let output = calc_water_level(1, vec![1., 3., 2., 1.]).unwrap();
        assert_eq!(output, vec![2.3333333333333335, 3.0, 2.8333333333333335, 2.8333333333333335]);
    }

    #[test]
    fn test11() {
        let output = calc_water_level(1, vec![1., 3., 2., 2.]).unwrap();
        assert_eq!(output, vec![3., 3., 3., 3.]);
    }

    #[test]
    fn test12() {
        let output = calc_water_level(1, vec![1., 3., 2., 3., 2.]).unwrap();
        assert_eq!(output, vec![3.2, 3.2, 3.2, 3.2, 3.2]);
    }

    #[test]
    fn it_works_for_custom_case() {
        let output = calc_water_level(1, vec![5., 0., 2.]).unwrap();
        assert_eq!(output, vec![5., 2.5, 2.5]);
    }

}

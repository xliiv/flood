use argh::FromArgs;
use flood;

#[derive(FromArgs, PartialEq, Debug)]
/// Calculates the water level for a given landscape after a given time.
///
/// Example:
/// Input: 1 5 0 2
/// This means 1 hour (unit) of rain on landscape 5 0 2
/// Output: 5 2.5 2.5
///
/// Because the water on 5 flows into 0. The water on 0 stays there. The water on 2 will partially
/// flow on 0. So the water level after 1 hour of rain on this landscape is 5 2.5 2.5
///
struct Config {
    /// given time unit to calculate the water level of the landscape
    #[argh(positional)]
    time: usize,

    #[argh(positional)]
    /// given segments of the landscape (e.g.: 5 0 2)
    landscape: Vec<f64>,
}


fn main() {
    let config: Config = argh::from_env();
    match flood::calc_water_level(config.time, config.landscape) {
        Ok(output) => {
            println!("Result water level is: {:?}", output);
        }
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(2);
        }
    }
}
